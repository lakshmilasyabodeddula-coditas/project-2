import getUsers from './fetchUsers.js';
import tableStructure from './createElement.js';
import insertFunctionality from './inserting/insert.js'
import updateFunctionality from './updating/update.js'
import createElement from './createElement.js';
import deleteElement from './deleting/delete.js';


const main = async () => {
    try {
        const studentList = await getUsers();
        const create = createElement['createElement'];
        const title = create("h1", "student management system", "colored");
           
        const createButton = document.createElement("button");
        createButton.textContent = "create";
        createButton.classList.add("creatButton")
        createButton.addEventListener("click", insertFunctionality['createForm']);
        document.getElementById("root").appendChild(createButton);

        const createTable = tableStructure['createTableStructure'];
        createTable();
        const createNewRow = tableStructure['createRow'];

        for (let index in studentList) {
            const row = createNewRow();

            let studentId = document.createElement("td");
            let studentName = document.createElement("td");
            let studentClass = document.createElement("td");
            let studentSchoolName = document.createElement("td");
            let studentArea = document.createElement("td");
            let studentCity = document.createElement("td");
            let studentState = document.createElement("td");
            
            studentId.textContent = studentList[index].studentId;
            studentName.textContent = studentList[index].studentName;
            studentClass.textContent = studentList[index].studentClass;
            studentSchoolName.textContent = studentList[index].studentSchoolName;
            studentArea.textContent = studentList[index]['studentAddress'].area;
            studentCity.textContent = studentList[index]['studentAddress'].city;
            studentState.textContent = studentList[index]['studentAddress'].state;

            row.append(studentId);
            row.append(studentName);
            row.append(studentClass);
            row.append(studentSchoolName);
            row.append(studentArea);
            row.append(studentCity);
            row.append(studentState);

            const editButton = document.createElement("button");
            editButton.textContent = "edit";
            editButton.setAttribute("id", studentId.textContent);
            row.append(editButton);
            let update = updateFunctionality['update'];
            editButton.addEventListener("click",update);

            const deleteButton = document.createElement("button");
            deleteButton.textContent = "delete";
            deleteButton.setAttribute("id", studentId.textContent);
            row.append(deleteButton);

            let deleteRecord= deleteElement['miniform'];
            deleteButton.addEventListener("click",deleteRecord);
        }
    } catch (e) {
        console.log(e);
    }
}
main();
export default {
    main
}