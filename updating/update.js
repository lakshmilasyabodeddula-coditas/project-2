import getUsers from "../fetchUsers.js";

function update(){
  const id=this.id;
  const getDetails= async () => {
    try{
        const studentList = await getUsers();
         generateFormwithFormData(id,studentList);
    }catch(e){
      console.log(e);
    }
  }
  getDetails();
}

function generateFormwithFormData(id,studentList)
{
  document.getElementsByTagName("table")[0].style.display="none";
  document.getElementsByClassName("creatButton")[0].style.display="none";
    for(let key in studentList)
    { 
      if(studentList[key].studentId==id)
      {       
        const studentId=studentList[key].studentId;
        const studentName=studentList[key].studentName;
        const studentClass=studentList[key].studentClass;
        const studentSchoolName=studentList[key].studentSchoolName;
        const studentAddress=studentList[key].studentAddress.addressId;
        const studentArea=studentList[key].studentAddress.area;
        const studentCity=studentList[key].studentAddress.city;
        const studentState=studentList[key].studentAddress.state;

        console.log(studentId);
        const formElement = document.createElement("form");
        formElement.setAttribute('method', 'post');
        document.getElementsByTagName("main")[0].appendChild(formElement);

        const idElement = document.createElement("label");
        idElement.textContent = "ID";
        idElement.setAttribute("for", "studentId");

        const idInput = document.createElement("input");
        idInput.setAttribute("type", "number");
        idInput.setAttribute("name", "studentId");
        idInput.setAttribute("id", "studentId");
        idInput.setAttribute("value",studentId);
        // idInput.setAttribute("ng-readonly","setReadonly");
        idInput.setAttribute("readonly","readonly");
        // readonly="readonly"

        const nameElement = document.createElement("label");
        nameElement.textContent = "Name";
        nameElement.setAttribute("for", "studentName");

        const nameInput = document.createElement("input");
        nameInput.setAttribute("type", "text");
        nameInput.setAttribute("name", "studentName");
        nameInput.setAttribute("id", "studentName");
        nameInput.setAttribute("value",studentName);


        const classElement = document.createElement("label");
        classElement.textContent = "Class";
        classElement.setAttribute("for", "studentClass");

        const classInput = document.createElement("input");
        classInput.setAttribute("type", "number");
        classInput.setAttribute("name", "studentClass");
        classInput.setAttribute("id", "studentClass");
        classInput.setAttribute("value",studentClass);

        const schoolElement = document.createElement("label");
        schoolElement.textContent = "school name";
        schoolElement.setAttribute("for", "studentSchoolName");

        const schoolInput = document.createElement("input");
        schoolInput.setAttribute("type", "text");
        schoolInput.setAttribute("name", "studentSchoolName");
        schoolInput.setAttribute("id", "studentSchoolName");
        schoolInput.setAttribute("value",studentSchoolName);

        const addressId = document.createElement("label");
        addressId.textContent = "address id";
        addressId.setAttribute("for", "addressId");

        const addressInput = document.createElement("input");
        addressInput.setAttribute("type", "number");
        addressInput.setAttribute("name", "addressId");
        addressInput.setAttribute("id", "addressId");
        addressInput.setAttribute("value",studentAddress);
        addressInput.setAttribute("readonly","readonly");

        const areaElement = document.createElement("label");
        areaElement.textContent = "area";
        areaElement.setAttribute("for", "area");

        const areaInput = document.createElement("input");
        areaInput.setAttribute("type", "text");
        areaInput.setAttribute("name", "area");
        areaInput.setAttribute("id", "area");
        areaInput.setAttribute("value",studentArea);

        const cityElement = document.createElement("label");
        cityElement.textContent = "city";
        cityElement.setAttribute("for", "city");

        const cityInput = document.createElement("input");
        cityInput.setAttribute("type", "text");
        cityInput.setAttribute("name", "city");
        cityInput.setAttribute("id", "city");
        cityInput.setAttribute("value",studentCity);

        const stateElement = document.createElement("label");
        stateElement.textContent = "state";
        stateElement.setAttribute("for", "state");

        const stateInput = document.createElement("input");
        stateInput.setAttribute("type", "text");
        stateInput.setAttribute("name", "state");
        stateInput.setAttribute("id", "state");
        stateInput.setAttribute("value",studentState);

        const button = document.createElement("button");
        button.type = 'submit'
        button.innerText = "submit";

        formElement.appendChild(idElement);
        formElement.appendChild(idInput);
        formElement.appendChild(nameElement);
        formElement.appendChild(nameInput);
        formElement.appendChild(classElement);
        formElement.appendChild(classInput);
        formElement.appendChild(schoolElement);
        formElement.appendChild(schoolInput);
        formElement.appendChild(addressId);
        formElement.appendChild(addressInput);
        formElement.appendChild(areaElement);
        formElement.appendChild(areaInput);
        formElement.appendChild(cityElement);
        formElement.appendChild(cityInput);
        formElement.appendChild(stateElement);
        formElement.appendChild(stateInput);
        formElement.appendChild(button);


        if (formElement.addEventListener('submit', async (e) => {
          document.getElementsByTagName("table")[0].style.display="block";
          document.getElementsByTagName("form")[0].style.display="none";
          location.reload();
          e.preventDefault();
          const id = document.getElementById("studentId").value;
          const name = document.getElementById("studentName").value;
          const class1 = document.getElementById("studentClass").value;
          const school = document.getElementById("studentSchoolName").value;
          const addressid = document.getElementById("addressId").value;
          const area = document.getElementById("area").value;
          const city = document.getElementById("city").value;
          const state = document.getElementById("state").value;
          const putDetails = await fetch("http://8134-103-51-153-190.ngrok.io/update", {
            method: "PUT",
            body: JSON.stringify({
              studentId:id,
              studentName:name,
              studentAddress: {
                addressId:addressid,
                city:city,
                area:area,
                state:state
              },
              studentSchoolName:school,
              studentClass:class1
            }),
            headers: new Headers({
              "ngrok-skip-browser-warning": "1234",
              'Content-type': 'application/json',
            }),
          }
      
          )
          console.log(await putDetails.json())
        }))

        return;
    }

}
}
export default {
    generateFormwithFormData,
    update
}
