
const table=document.createElement("table");
const thead=document.createElement("thead");
const tbody=document.createElement("tbody");
const body=document.getElementById("root");

function createElement(tagName,content="",className)
{
    const elementName=document.createElement(tagName);
    elementName.textContent=content;
    document.getElementById("root").appendChild(elementName);
    elementName.classList.add(className);
}
function createTableStructure()
{
    table.appendChild(thead);
    table.appendChild(tbody);
    const tr=document.createElement("tr");
    const tableHeadingId=document.createElement("th");
    tableHeadingId.textContent="ID";
    const tableHeadingName=document.createElement("th");
    tableHeadingName.textContent="NAME";
    const tableHeadingClass=document.createElement("th");
    tableHeadingClass.textContent="CLASS";
    const tableHeadingSchool=document.createElement("th");
    tableHeadingSchool.textContent="SCHOOL"
    const tableHeadingArea=document.createElement("th");
    tableHeadingArea.textContent="AREA";
    const tableHeadingCity=document.createElement("th");
    tableHeadingCity.textContent="CITY";
    const tableHeadingState=document.createElement("th");
    tableHeadingState.textContent="STATE";
    thead.appendChild(tr);
    tr.appendChild(tableHeadingId);
    tr.appendChild(tableHeadingName);
    tr.appendChild(tableHeadingClass);
    tr.appendChild(tableHeadingSchool);
    tr.appendChild(tableHeadingArea);
    tr.appendChild(tableHeadingCity);
    tr.appendChild(tableHeadingState);
    document.getElementById("root").appendChild(table);
}

function createRow()
{
    const trow=document.createElement("tr");
    tbody.appendChild(trow);    
    return trow;
}

export default {
    createTableStructure,
    createRow,
    createElement
}