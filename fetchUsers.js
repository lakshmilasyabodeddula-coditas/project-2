const getUsers = async()=>{
    try{
    const response=await fetch("http://8134-103-51-153-190.ngrok.io/students",{
        method: "get",
        headers: new Headers({
          "ngrok-skip-browser-warning": "1234",
        }),
      });
    const object=await response.json();
    return object;
    }
    catch(e)
    {
        console.log(e);
    }
}
export default getUsers;