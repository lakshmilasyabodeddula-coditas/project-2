import abc from "../index.js";

function miniform()
{
    let studentId=this.id;
    const formElement=document.createElement("form");
    formElement.classList.add("miniform");

    const question=document.createElement("label");
    question.textContent="Are you sure you want to delete?";

    const yesButton=document.createElement("button");
    yesButton.textContent="yes";
    yesButton.style.display="block";

    const noButton=document.createElement("button");
    noButton.textContent="no";

    document.body.appendChild(formElement);
    formElement.appendChild(question);
    formElement.appendChild(yesButton);
    formElement.appendChild(noButton);

   yesButton.addEventListener("click",async ()=>{
    try{
       
        const response = await fetch(`http://8134-103-51-153-190.ngrok.io/deletestudent/${studentId}` ,{
        method: 'DELETE',
        headers: new Headers({
            "ngrok-skip-browser-warning": "1234",
            'Content-type': 'application/json',
          }),
    }
    );
    console.log("deleting2");
     }catch(e){
        console.log(e);
     }})
    
    noButton.addEventListener("click",()=>{
        formElement.style.display="none";
    })
}

export default{
   
    miniform
}